MorAPIon
========

API *REST* pour la gestion de parties de morpion. Cette API est à visée
pédagogique.

Installée par le formateur sur un domaine accessible, elle permetra aux
stagiaires de s'initier à JavaScript, AJAX, aux *single-page-apps*...

Installation
------------

    apt install python3-virtualenv virtualenvwrapper
    mkvirtualenv --python /usr/bin/python3 morapion
    pip install -r requirements.txt
    ./manage.py migrate

Lancement
---------

    ./manage.py runserver

L'API est ensuite disponible sur http://localhost:8000 (nb: elle ne contient
pour l'instant aucune donnée).


Une administration est disponible (ex: pour effacer des parties) sur http://localhost:8000/admin , elle
nécessite de créer un utilisateur en ligne de commande :

    ./manage.py createsuperuser


Mise en production
------------------

```
echo 'ALLOWED_HOSTS=["myhost.example.com"]' > morapion/local_settings.py
echo 'DEBUG=False' >> morapion/local_settings.py
```

Changer `SECRET_KEY` également.

Documentation de l'API
-----------------------

Cette API héberge des parties de Morpion. Elle gère la logique de jeu et permet
la récupération et la modification de l'état du jeu par les deux joueurs, via
le réseau.

Les parties sont définies par :

- un identifiant, choisi librement (composé de caractères et de tirets)
- un mot de passe, permettant de limiter l'accès

Les joueurs sont nommés par leur symbole : '`x`' et `o`.

Les cellules sont nommées par un système de coordonnées :

        A   B   C
      ╭───┬───┬───┐
    1 │ x │   │   │
      ├───┼───┼───┤
    2 │   │   │   │
      ├───┼───┼───┤
    3 │   │ o │   │
      └───┴───┴───┘

Dans cet exemple la cellule `A1` contient la valeur `'x'` et la cellule `B3`
contient la valeur `'o'`. Les autres contiennent `null`.


### Récupérer la liste des parties

- méthode HTTP : `GET`
- URL : `/games`

Format :

    [
      {
        "identifier": "gentlegame",
        "finished": true,
        "winner": "x",
      },
      {
        "identifier": "evilgame",
        "finished": false,
        "winner": null,
      },
    ]

On peut également ne récupérer que les parties en cours :


- méthode HTTP : `GET`
- URL : `/games?active`

… Ou uniquement les parties terminées

- méthode HTTP : `GET`
- URL : `/games?finished`

### Récupérer l'état d'une partie

Ne nécessite pas de mot de passe.

- méthode HTTP : `GET`
- URL : `/games/:identifier`

*NB: `:identifier` est à remplacer par l'identfiant de la partie.

Format :

    {
      "identifier": "gentlegame",
      "finished": true,
      "turn": "o",
      "winner": "x",
      "cells" : {
        "A1" : "x",
        "A2" : "o",
        "A3" : "o",
        "B1" : "x",
        "B2" : "o",
        "B3" : "o",
        "C1" : "x",
        "C2" : "o",
        "C3" : "o"
      }
    }

### Créer une partie

C'est à vous d'envoyer un contenu lors de la requête POST :

- méthode HTTP : `POST`
- URL : `/games`

Il doit être en JSON au format suivant :

    {
      "identifier": "le-nom-de-ma-partie",
      "password": "mypassword"
    }

Les codes de retour HTTP possibles sont :

- *201* : partie créée et accessible.
- *400* : données soumises invalides
- *500* : erreur serveur

Dans tous les cas, le serveur renvoie un contenu JSON contenant un message
explicatif, par exemple :

    {
      "msg": "Une partie avec ce nom existe déjà"
    }


### Jouer un coup

Nécessite un mot de passe (cf ci-après).

- méthode HTTP: `POST`
- URL : `/games/:identifier/play`

Exemple de JSON à envoyer (pour que le joueur *x* joue en A3) :

    {
      "password": "coucou",
      "player": "x",
      "cell": "A3"
    }

Les codes de retour HTTP possibles sont :

- *200* : Le coup a été enregistré
- *400* : invalide
- *401* : mauvais mot de passe
- *403* : pas au tour de ce joueur de jouer
- *500* : erreur serveur
