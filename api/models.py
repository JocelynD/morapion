from django.db import models
from django.db.models import Q, Count

PLAYERS = ('x', 'o')
PLAYERS_CHOICES = ((i, i) for i in PLAYERS)


class Game(models.Model):
    identifier = models.SlugField(max_length=50, unique=True)
    password = models.CharField(max_length=100)
    finished = models.BooleanField(default=False)
    turn = models.CharField(choices=PLAYERS_CHOICES, max_length=1, default='x')
    winner = models.CharField(choices=PLAYERS_CHOICES, blank=True, max_length=1)

    def __str__(self):
        return self.identifier

    def init_cells(self):
        for i in ('A', 'B', 'C'):
            for j in ('1', '2', '3'):
                Cell.objects.create(game=self, col=i, row=j)

    def is_finished(self):
        return (
            self.has_won('x')
            or self.has_won('o')
            or (self.cells.filter(value='').count() == 0))  # equality

    def has_won(self, player):
        cells = self.cells.filter(value=player)
        if cells.count() < 3:
            return False
        else:
            cols = cells.values('col').annotate(Count('col')).filter(
                col__count__gte=3)
            rows = cells.values('row').annotate(Count('row')).filter(
                row__count__gte=3)
            diag1 = cells.filter(
                Q(col='A', row='1') |
                Q(col='B', row='2') |
                Q(col='C', row='3'))

            diag2 = cells.filter(
                Q(col='A', row='3') |
                Q(col='B', row='2') |
                Q(col='C', row='1'))

        return (cols.exists() or rows.exists()
                or (diag1.count() >= 3) or (diag2.count() >= 3))

    def serialize(self):
        return {
            'identifier': self.identifier,
            'turn': self.turn,
            'finished': self.finished,
            'winner': self.winner or None,
            'cells': {'{}{}'.format(i.col, i.row): i.value or None
                      for i in self.cells.all()}
        }


class Cell(models.Model):
    game = models.ForeignKey(Game, null=False, related_name='cells')
    col = models.CharField(
        max_length=1,
        choices=[('A', 'A'), ('B', 'B'), ('C', 'C')], blank=False)
    row = models.CharField(
        max_length=1,
        choices=[('1', '1'), ('2', '2'), ('3', '3')], blank=False)
    value = models.CharField(choices=PLAYERS_CHOICES, max_length=1)

    class Meta:
        unique_together = ['game', 'col', 'row']


    def __str__(self):
        return '{}{}'.format(self.col, self.row)
