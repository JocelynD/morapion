import json

from django.test import TestCase

from .models import Game


class GameTestCase(TestCase):
    def setUp(self):
        a = Game.objects.create(identifier='a', password='a')
        a.init_cells()
        a2 = a.cells.get(col='A', row='2')
        a2.value = 'o'
        a2.save()

    def testGameVictoryCol(self):
        game = Game.objects.first()
        game.cells.filter(col='A').update(value='o')
        self.assertTrue(game.has_won('o'))
        self.assertFalse(game.has_won('x'))

    def testGameVictoryRow(self):
        game = Game.objects.first()
        game.cells.filter(row='2').update(value='o')
        self.assertTrue(game.has_won('o'))
        self.assertFalse(game.has_won('x'))

    def testGameVictoryDiag1(self):
        game = Game.objects.first()
        game.cells.update(value='')
        game.cells.filter(col='A', row='1').update(value='x')
        game.cells.filter(col='B', row='2').update(value='x')
        game.cells.filter(col='C', row='3').update(value='x')

        self.assertFalse(game.has_won('o'))
        self.assertTrue(game.has_won('x'))

    def testGameVictoryDiag2(self):
        game = Game.objects.first()
        game.cells.update(value='')
        game.cells.filter(col='A', row='3').update(value='x')
        game.cells.filter(col='B', row='2').update(value='x')
        game.cells.filter(col='C', row='1').update(value='x')

        self.assertFalse(game.has_won('o'))
        self.assertTrue(game.has_won('x'))

    def testGameEquality(self):
        game = Game.objects.first()
        game.cells.update(value='')
        game.cells.filter(col='A', row='1').update(value='x')
        game.cells.filter(col='A', row='2').update(value='o')
        game.cells.filter(col='A', row='3').update(value='x')

        game.cells.filter(col='B', row='1').update(value='x')
        game.cells.filter(col='B', row='2').update(value='o')
        game.cells.filter(col='B', row='3').update(value='o')

        game.cells.filter(col='C', row='1').update(value='o')
        game.cells.filter(col='C', row='2').update(value='x')
        game.cells.filter(col='C', row='3').update(value='o')

        self.assertFalse(game.has_won('o'))
        self.assertFalse(game.has_won('x'))
        self.assertTrue(game.is_finished())


class APITestCase(TestCase):
    def setUp(self):
        a = Game.objects.create(identifier='a', password='a')
        a.init_cells()
        a2 = a.cells.get(col='A', row='2')
        a2.value = 'o'
        a2.save()

    def testListGame(self):
        response = self.client.get('/games')
        self.assertEqual(response.status_code, 200)
        self.assertEqual(
            response.json(),
            [{
                'identifier': 'a',
                'finished': False,
                'turn': 'x',
                'winner': None,
                'cells': {
                    'A1': None, 'A2': 'o', 'A3': None,
                    'B1': None, 'B2': None, 'B3': None,
                    'C1': None, 'C2': None, 'C3': None
                }
            }]
        )
        response = self.client.get('/games?finished')
        self.assertEqual(response.status_code, 200)
        self.assertEqual(len(response.json()), 0)

        response = self.client.get('/games?active')
        self.assertEqual(response.status_code, 200)
        self.assertEqual(len(response.json()), 1)

    def testCreateGame(self):
        # Wrong player
        resp = self.client.post('/games', json.dumps({
            'identifier': 'foooo',
            'password': 'baaaaar',
        }), content_type="application/json")

        self.assertEqual(resp.status_code, 201)

    def testCreateGameBadName(self):
        # Wrong player
        resp = self.client.post('/games', json.dumps({
            'identifier': 'trala li lou',
            'password': 'baaaaar',
        }), content_type="application/json")

        self.assertEqual(resp.status_code, 400)

    def testCreateGameBadContent(self):
        # Wrong player
        resp = self.client.post('/games', json.dumps({
            'frseia': 'trala li lou',
            'password': 'baaaaar',
        }), content_type="application/json")

        self.assertEqual(resp.status_code, 400)


    def testPlayGame(self):
        # Wrong player
        resp = self.client.post('/games/a/play', json.dumps({
            'player': 'o',
            'cell': 'A1',
            'password': 'a',
        }), content_type="application/json")

        self.assertEqual(resp.status_code, 403)

        # Bad password
        resp = self.client.post('/games/a/play', json.dumps({
            'player': 'o',
            'cell': 'A1',
            'password': 'aaa',
        }), content_type="application/json")

        self.assertEqual(resp.status_code, 401)


        # Case prise
        resp = self.client.post('/games/a/play', json.dumps({
            'player': 'x',
            'cell': 'A2',
            'password': 'a',
        }), content_type="application/json")

        self.assertEqual(resp.status_code, 400)

        # Format de cellule mauvais

        resp = self.client.post('/games/a/play', json.dumps({
            'player': 'x',
            'cell': '',
            'password': 'a',
        }), content_type="application/json")
        self.assertEqual(resp.status_code, 400)

        resp = self.client.post('/games/a/play', json.dumps({
            'player': 'x',
            'cell': 'V6',
            'password': 'a',
        }), content_type="application/json")
        self.assertEqual(resp.status_code, 400)


        # Ok
        resp = self.client.post('/games/a/play', json.dumps({
            'player': 'x',
            'cell': 'A1',
            'password': 'a',
        }), content_type="application/json")
        self.assertEqual(resp.status_code, 200)

        resp = self.client.post('/games/a/play', json.dumps({
            'player': 'o',
            'cell': 'B2',
            'password': 'a',
        }), content_type="application/json")

        resp = self.client.post('/games/a/play', json.dumps({
            'player': 'x',
            'cell': 'A3',
            'password': 'a',
        }), content_type="application/json")
        self.assertEqual(resp.status_code, 200)

        game = Game.objects.first()
        self.assertEqual(game.finished, False)
        self.assertEqual(game.winner, '')

        # Puts an end to the game
        resp = self.client.post('/games/a/play', json.dumps({
            'player': 'o',
            'cell': 'C2',
            'password': 'a',
        }), content_type="application/json")
        self.assertEqual(resp.status_code, 200)

        game = Game.objects.first()
        self.assertTrue(game.finished)
        self.assertEqual(game.winner, 'o')
