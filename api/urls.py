
from django.conf.urls import url, include
from django.contrib import admin

import api

from .views import list_games, create_game, play_move, get_game

urlpatterns = [
    url(r'^games$', list_games),
    url(r'^games/(?P<slug>[\w-]+)$', get_game),
    url(r'^games/(?P<slug>[\w-]+)/play$', play_move),
]
