import json
import re

from django.http.response import JsonResponse, HttpResponse
from django.shortcuts import get_object_or_404
from django.views.decorators.csrf import csrf_exempt


from .models import PLAYERS, Game, Cell


class JsonMsg(JsonResponse):
    def __init__(self, message, *args, **kwargs):
        kwargs['data'] = {'msg': message}
        super().__init__(*args, **kwargs)


@csrf_exempt
def play_move(request, slug):
    try:
        d = json.loads(request.body.decode())
    except ValueError:
        return JsonMsg('Malformed JSON', status=400)

    player = d.get('player')
    cell = d.get('cell')
    password = d.get('password')

    game = get_object_or_404(Game, identifier=slug)

    if password != game.password:
        return JsonMsg('Mauvais mot de passe', status=401)
    elif game.turn != player:
        return JsonMsg('Pas ton tour de jouer !', status=403)

    elif game.finished:
        return JsonMsg('Partie terminée', status=400)

    elif player not in PLAYERS:
        return JsonMsg('Joueur invalide', status=400)

    elif len(cell) != 2:
        return JsonMsg('Cellule invalide', status=400)

    col, row = cell.upper()
    try:
        cell = game.cells.get(col=col, row=row)
    except Cell.DoesNotExist:
        return JsonMsg('Cellule invalide', status=400)
    else:
        if cell.value != '':
            return JsonMsg('Cellule non vide', status=400)

        cell.value = player
        cell.save()
        if player == 'x':
            game.turn = 'o'
        else:
            game.turn = 'x'

        if game.has_won(player):
            game.finished = True
            game.winner = player

        game.save()
        return JsonMsg('Coup enregistré', status=200)


def get_game(request, slug):
    game = get_object_or_404(Game, identifier=slug)
    return JsonResponse(game.serialize(), safe=False)


@csrf_exempt
def create_game(request):
    try:
        d = json.loads(request.body.decode())

    except ValueError:
        return JsonMsg('Malformed JSON', status=400)

    identifier = d.get('identifier', '')
    password = d.get('password', '')

    if not re.match(r'^[\w-]+$', identifier):
        return JsonMsg(
            'Les noms de partie valides comportent des lettres, des chiffres et des "-" uniquement',
            status=400)
    if Game.objects.filter(identifier=identifier).exists():
        return JsonMsg(
            'Une partie avec ce nom existe', status=400)
    elif identifier == '' or password == '':
        return JsonMsg(
            'password et identifier doivent être remplis', status=400)
    else:
        g = Game.objects.create(identifier=identifier, password=password)
        g.init_cells()
        return JsonMsg('Partie créée', status=201)


@csrf_exempt
def list_games(request):
    if request.method == 'POST':
        return create_game(request)

    games = Game.objects
    if 'active' in request.GET:
        games = games.filter(finished=False)
    if 'finished' in request.GET:
        games = games.filter(finished=True)
    return JsonResponse([i.serialize() for i in games.all()], safe=False)
